package com.interview.screenmedia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.screenmedia.entities.FileData;
import com.interview.screenmedia.entities.RequestModel;
import com.interview.screenmedia.entities.ResponseModel;
import com.interview.screenmedia.util.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest
public class
ServiceTest {
	
	@Spy
	Service serviceSpy;

	@Autowired
	RequestModel requestModel;

	@Autowired
	ResponseModel responseModel;

	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void calculateFuelCostTest(){
		String expectedValue = "1.37";
		requestModel.setDate("1/1/2018");
		requestModel.setMpg("30");
		requestModel.setMileage("450");
		requestModel.setFuelType("petrol");

		//Date todaysDate = new Date();
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date journeyDate = null;
		try {
			journeyDate = formatter.parse(requestModel.getDate());
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(100);
		}
		FileData fileData = new FileData();
		fileData.setDate(journeyDate);
		fileData.setULSP_Pump(120.95);
		fileData.setULSD_Pump(123.51);
		fileData.setULSP_Duty(57.95);
		fileData.setULSD_Duty(57.95);
		fileData.setULSP_Vat(20);
		fileData.setULSD_Vat(20);

		//Date todaysDate = new Date();
		DateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
		Date todaysDate = null;
		try {
			todaysDate = formatter1.parse(formatter1.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(100);
		}
		FileData fileData1 = new FileData();
		fileData1.setDate(todaysDate);
		fileData1.setULSP_Pump(118.94);
		fileData1.setULSD_Pump(120.48);
		fileData1.setULSP_Duty(57.00);
		fileData1.setULSD_Duty(57.00);
		fileData1.setULSP_Vat(20);
		fileData1.setULSD_Vat(20);


		Map<Date, FileData> fileDataMappedWithDate = new HashMap<Date, FileData> ();
		fileDataMappedWithDate.put(journeyDate, fileData);
		fileDataMappedWithDate.put(todaysDate, fileData1);

		Mockito.doReturn(fileDataMappedWithDate).when(serviceSpy).readDataFromFile();
		Object object = serviceSpy.calculateFuelCost(requestModel);
		responseModel = (ResponseModel) object;
		String actual = String.valueOf(responseModel.getFuelCostSavingsInPounds());
		assertEquals(expectedValue,actual);
	}
}
