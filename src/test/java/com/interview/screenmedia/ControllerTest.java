package com.interview.screenmedia;


import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.interview.screenmedia.entities.RequestModel;
import com.interview.screenmedia.entities.ResponseModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringRunner.class)
@WebMvcTest(value = Controller.class, secure = false)
public class ControllerTest{

	@Autowired
	private MockMvc mockMvc;

	@Value( "${currencyConversionFactor}" )
	private String currencyConversionFactor;

	@MockBean
	private Service service;

	@Autowired
	ObjectMapper objectMapper;

	String exampleFuelJson = "{\"date\":\"29/1/2018\",\"fuelType\":\"petrol\",\"mpg\":\"20\",\"mileage\":\"300\" }";

	@Test
	public void getFuelCost() throws Exception {
		double conversionFactor = Double.parseDouble(currencyConversionFactor);
		//BigDecimal conversionFactor = new BigDecimal(currencyConversionFactor);
		RequestModel requestModel = new RequestModel();
		//Date date=new Date();
		requestModel.setDate("29/1/2018");
		requestModel.setFuelType("petrol");
		requestModel.setMpg("20");
		requestModel.setMileage("300");
		ResponseModel responseModelMock =  new ResponseModel();
		responseModelMock.setTotalFuelCostInPounds(100*conversionFactor);
		responseModelMock.setTotalDutyCostInPounds(10*conversionFactor);
		responseModelMock.setTotalFuelCostInPoundsAsOnToday(90*conversionFactor);
		responseModelMock.setTotalDutyCostInPoundsAsOnToday(9*conversionFactor);
		responseModelMock.setFuelCostSavingsInPounds(10*conversionFactor);


		// Service.getData to respond back with responseModel
		Mockito.when(
				service.calculateFuelCost(
						Mockito.any(RequestModel.class))).thenReturn(responseModelMock);// idi only response mock chestunam ...test case kadhu ///idi

		// Send requestModel as body to ""
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/fuelCost")
				.accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(requestModel)) //idi actual test case- json string pass chestundi
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();
		//System.out.println("******************"+response.toString());
		assertEquals(HttpStatus.OK.value(),response.getStatus());

		//System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		mockMvc.perform(requestBuilder)
		.andExpect((ResultMatcher) jsonPath("$.totalFuelCostInPounds").exists())
		.andExpect((ResultMatcher) jsonPath("$.totalDutyCostInPounds").exists())
		.andExpect((ResultMatcher) jsonPath("$.totalFuelCostInPoundsAsOnToday").exists())
		.andExpect((ResultMatcher) jsonPath("$.totalDutyCostInPoundsAsOnToday").exists())
		.andExpect((ResultMatcher) jsonPath("$.fuelCostSavingsInPounds").exists())
		.andExpect((ResultMatcher) jsonPath("$.totalFuelCostInPounds").value(100*conversionFactor))
		.andExpect((ResultMatcher) jsonPath("$.totalDutyCostInPounds").value(10*conversionFactor))
		.andExpect((ResultMatcher) jsonPath("$.totalFuelCostInPoundsAsOnToday").value(90*conversionFactor))
		.andExpect((ResultMatcher) jsonPath("$.totalDutyCostInPoundsAsOnToday").value(9*conversionFactor))
		.andExpect((ResultMatcher) jsonPath("$.fuelCostSavingsInPounds").value(10*conversionFactor))
		.andDo(print());
	}
}
