package com.interview.screenmedia.entities;

import org.springframework.stereotype.Component;

@Component
public class ResponseModel {

	private double totalFuelCostInPounds ;
	private double totalDutyCostInPounds;
	private double totalFuelCostInPoundsAsOnToday;
	private double totalDutyCostInPoundsAsOnToday;
	private double fuelCostSavingsInPounds;


	public double getTotalFuelCostInPounds() {
		return totalFuelCostInPounds;
	}

	public void setTotalFuelCostInPounds(double totalFuelCostInPounds) {
		this.totalFuelCostInPounds = totalFuelCostInPounds;
	}

	public double getTotalDutyCostInPounds() {
		return totalDutyCostInPounds;
	}

	public void setTotalDutyCostInPounds(double totalDutyCostInPounds) {
		this.totalDutyCostInPounds = totalDutyCostInPounds;
	}


	public double getFuelCostSavingsInPounds() {
		return fuelCostSavingsInPounds;
	}

	public double getTotalFuelCostInPoundsAsOnToday() {
		return totalFuelCostInPoundsAsOnToday;
	}

	public void setTotalFuelCostInPoundsAsOnToday(double totalFuelCostInPoundsAsOnToday) {
		this.totalFuelCostInPoundsAsOnToday = totalFuelCostInPoundsAsOnToday;
	}

	public double getTotalDutyCostInPoundsAsOnToday() {
		return totalDutyCostInPoundsAsOnToday;
	}

	public void setTotalDutyCostInPoundsAsOnToday(double totalDutyCostInPoundsAsOnToday) {
		this.totalDutyCostInPoundsAsOnToday = totalDutyCostInPoundsAsOnToday;
	}

	public void setFuelCostSavingsInPounds(double fuelCostSavingsInPounds) {
		this.fuelCostSavingsInPounds = fuelCostSavingsInPounds;
	}

	@Override
	public String toString() {
		return "ResponseModel{" +
				"totalFuelCostInPounds=" + totalFuelCostInPounds +
				", totalDutyCostInPounds=" + totalDutyCostInPounds +
				", totalFuelCostInPoundsAsOnToday=" + totalFuelCostInPoundsAsOnToday +
				", totalDutyCostInPoundsAsOnToday=" + totalDutyCostInPoundsAsOnToday +
				", fuelCostSavingsInPounds=" + fuelCostSavingsInPounds +
				'}';
	}



}
