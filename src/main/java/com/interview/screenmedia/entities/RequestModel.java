package com.interview.screenmedia.entities;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;

@Component
public class RequestModel {
		@NotNull
		//@PastOrPresent
		//@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
		//public Date date;
		private String date;
		@NotNull
		//@Positive
		private String mpg;


		@NotNull
		//@Positive
		private String mileage;
		@NotNull
		private String fuelType;


		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getMpg() {
			return mpg;
		}

		public void setMpg(String mpg) {
			this.mpg = mpg;
		}

		public String getMileage() {
			return mileage;
		}

		public void setMileage(String mileage) {
			this.mileage = mileage;
		}

		public String getFuelType() {
			return fuelType;
		}

		public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

		@Override
		public String toString() {
			return "RequestModel{" +
					"date='" + date + '\'' +
					", mpg='" + mpg + '\'' +
					", mileage='" + mileage + '\'' +
					", fuelType='" + fuelType + '\'' +
					'}';
		}



}
