package com.interview.screenmedia.entities;

import java.util.Date;

public class FileData {
	private Date date;
	private double ULSP_Pump;
	private double ULSD_Pump;
	private double ULSP_Duty;
	private double ULSD_Duty;
	private double ULSP_Vat;
	private double ULSD_Vat;
	
	public FileData() {
		super();
	}
	
	public FileData(Date date, double uLSP_Pump, double uLSD_Pump, double uLSP_Duty, double uLSD_Duty, double uLSP_Vat,
			double uLSD_Vat) {
		super();
		this.date = date;
		ULSP_Pump = uLSP_Pump;
		ULSD_Pump = uLSD_Pump;
		ULSP_Duty = uLSP_Duty;
		ULSD_Duty = uLSD_Duty;
		ULSP_Vat = uLSP_Vat;
		ULSD_Vat = uLSD_Vat;
	}


	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getULSP_Pump() {
		return ULSP_Pump;
	}
	public void setULSP_Pump(double uLSP_Pump) {
		ULSP_Pump = uLSP_Pump;
	}
	public double getULSD_Pump() {
		return ULSD_Pump;
	}
	public void setULSD_Pump(double uLSD_Pump) {
		ULSD_Pump = uLSD_Pump;
	}
	public double getULSP_Duty() {
		return ULSP_Duty;
	}
	public void setULSP_Duty(double uLSP_Duty) {
		ULSP_Duty = uLSP_Duty;
	}
	public double getULSD_Duty() {
		return ULSD_Duty;
	}
	public void setULSD_Duty(double uLSD_Duty) {
		ULSD_Duty = uLSD_Duty;
	}
	public double getULSP_Vat() {
		return ULSP_Vat;
	}
	public void setULSP_Vat(double uLSP_Vat) {
		ULSP_Vat = uLSP_Vat;
	}
	public double getULSD_Vat() {
		return ULSD_Vat;
	}
	public void setULSD_Vat(double uLSD_Vat) {
		ULSD_Vat = uLSD_Vat;
	}
	
	
	@Override
	public String toString() {
		return "FileData [date=" + date + ", ULSP_Pump=" + ULSP_Pump + ", ULSD_Pump=" + ULSD_Pump + ", ULSP_Duty="
				+ ULSP_Duty + ", ULSD_Duty=" + ULSD_Duty + ", ULSP_Vat=" + ULSP_Vat + ", ULSD_Vat=" + ULSD_Vat + "]";
	}
	
}
