package com.interview.screenmedia;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.interview.screenmedia.entities.FileData;
//import com.interview.screenmedia.inputFiles;
import com.interview.screenmedia.entities.RequestModel;
import com.interview.screenmedia.entities.ResponseModel;
import com.interview.screenmedia.util.ServiceException;

import java.io.*;
//import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.time.format.DateTimeFormatter;
//import java.time.format.DateTimeParseException;
import java.util.*;

@Component
public class Service {

	private static final double poundToPenceCF = 0.01;
	private static final double gallonToLitreCF = 0.21997;
	private static final DecimalFormat df = new DecimalFormat("#.##");

	@Value("${currencyConversionFactor}")
	private String currencyConversionFactor;

	private double totalFuelCostInPence;
	private double totalDutyCostInPence;
	private double totalFuelCostInPenceToday;
	private double totalDutyCostInPenceToday;
	private double fuelCostSavingsInPence;
	private double milesPerLiter;
	private double numberOfLitersConsumed;

	public Object calculateFuelCost(RequestModel requestObject) {
		Map<Date, FileData> fileDataMappedWithDate = readDataFromFile();
		Date journeyDate = validateJourneyDate(requestObject, fileDataMappedWithDate);
		validateMpg(requestObject);
		validateMileage(requestObject);
		validateFuelTypeForGivenJourneyDate(requestObject, fileDataMappedWithDate, journeyDate);
		Date todaysDate = getTodaysDate();
		retrieveTodaysDateFromInputFile(requestObject, fileDataMappedWithDate, todaysDate);
		return mapResponseModel();
	}

	private void retrieveTodaysDateFromInputFile(RequestModel requestObject, Map<Date, FileData> fileDataMappedWithDate, Date todaysDate) {
		if (fileDataMappedWithDate.get(todaysDate) != null) {
			validateFuelTypeForTheCurrentDayJourney(requestObject, fileDataMappedWithDate, todaysDate);
			fuelCostSavingsInPence = totalFuelCostInPence - totalFuelCostInPenceToday;

		} else {
			throw new ServiceException(102);
		}
	}

	private Date getTodaysDate() {
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date todaysDate = null;
		try {
			todaysDate = formatter.parse(formatter.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(100);
		}
		System.out.println("today....." + todaysDate);
		return todaysDate;
	}

	private ResponseModel mapResponseModel() {
		ResponseModel responseModel = new ResponseModel();
		responseModel.setTotalFuelCostInPounds(Double.valueOf(df.format(totalFuelCostInPence * poundToPenceCF)));
		responseModel.setTotalDutyCostInPounds(Double.valueOf(df.format(totalDutyCostInPence * poundToPenceCF)));
		responseModel.setTotalFuelCostInPoundsAsOnToday(Double.valueOf(df.format(totalFuelCostInPenceToday * poundToPenceCF)));
		responseModel.setTotalDutyCostInPoundsAsOnToday(Double.valueOf(df.format(totalDutyCostInPenceToday * poundToPenceCF)));
		responseModel.setFuelCostSavingsInPounds(Double.valueOf(df.format(fuelCostSavingsInPence * poundToPenceCF)));
		return responseModel;
	}

	private void validateFuelTypeForTheCurrentDayJourney(RequestModel requestObject, Map<Date, FileData> fileDataMappedWithDate, Date todaysDate) {
		//FuelType-Petrol
		if (requestObject.getFuelType().equalsIgnoreCase("PETROL")) {
			totalFuelCostInPenceToday = (fileDataMappedWithDate.get(todaysDate).getULSP_Pump()) * (numberOfLitersConsumed);
			totalDutyCostInPenceToday = (fileDataMappedWithDate.get(todaysDate).getULSP_Duty()) * (numberOfLitersConsumed);
		}
		//FuelType-Diesel
		else if (requestObject.getFuelType().equalsIgnoreCase("DIESEL")) {
			totalFuelCostInPenceToday = (fileDataMappedWithDate.get(todaysDate).getULSD_Pump()) * (numberOfLitersConsumed);
			totalDutyCostInPenceToday = (fileDataMappedWithDate.get(todaysDate).getULSD_Duty()) * (numberOfLitersConsumed);
		} else {
			throw new ServiceException(106);
		}
	}

	private void validateFuelTypeForGivenJourneyDate(RequestModel requestObject, Map<Date, FileData> fileDataMappedWithDate, Date journeyDate) {
		//CASE1-PETROL
		if (requestObject.getFuelType().equalsIgnoreCase("PETROL")) {
			totalFuelCostInPence = (fileDataMappedWithDate.get(journeyDate).getULSP_Pump()) * (numberOfLitersConsumed);
			totalFuelCostInPence = Double.valueOf(df.format(totalFuelCostInPence));
			totalDutyCostInPence = (fileDataMappedWithDate.get(journeyDate).getULSP_Duty()) * (numberOfLitersConsumed);
		}
		//CASE2-DIESEL
		else if (requestObject.getFuelType().equalsIgnoreCase("DIESEL")) {
			totalFuelCostInPence = (fileDataMappedWithDate.get(journeyDate).getULSD_Pump()) * (numberOfLitersConsumed);
			totalFuelCostInPence = Double.valueOf(df.format(totalFuelCostInPence));
			totalDutyCostInPence = (fileDataMappedWithDate.get(journeyDate).getULSD_Duty()) * numberOfLitersConsumed;
		} else {
			throw new ServiceException(106);
		}
	}

	private void validateMileage(RequestModel requestObject) {
		if (!requestObject.getMileage().isEmpty()) {
			String mileage = requestObject.getMileage();
			char[] chars = mileage.toCharArray();
			for (char c : chars) {
				if (!Character.isDigit(c)) {
					throw new ServiceException(110);
				}
			}
			numberOfLitersConsumed = validateNumLtrsConsumed(mileage, milesPerLiter);
		} else {
			throw new ServiceException(108);
		}
		if (numberOfLitersConsumed <= 0) {
			throw new ServiceException(105);
		}
	}

	private void validateMpg(RequestModel requestObject) {
		if (!requestObject.getMpg().isEmpty()) {
			String milesPerGallon = requestObject.getMpg();
			char[] chars = milesPerGallon.toCharArray();
			for (char c : chars) {
				if (!Character.isDigit(c)) {
					throw new ServiceException(109);
				}
			}
			milesPerLiter = validateMilesPerLiter(milesPerGallon);
		} else {
			throw new ServiceException(107);
		}
		if (milesPerLiter <= 0) {
			throw new ServiceException(104);
		}
	}

	private Date validateJourneyDate(RequestModel requestObject, Map<Date, FileData> fileDataMappedWithDate) {
		Date journeyDate = new Date(0);
		boolean isDateFormatValid = true;
		isDateFormatValid = validateDateFormat(requestObject, isDateFormatValid);
		try {
			journeyDate = new SimpleDateFormat("dd/MM/yyyy").
					parse(requestObject.getDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (fileDataMappedWithDate.get(journeyDate) == null || !isDateFormatValid
				|| requestObject.getDate().length() > 10 ||
				!Character.isDigit(requestObject.getDate().charAt(requestObject.getDate().length() - 1))
		) {
			throw new ServiceException(103);
		}
		return journeyDate;
	}

	private boolean validateDateFormat(RequestModel requestObject, boolean isDateFormatValid) {
		String[] elements = requestObject.getDate().split("/");
		for (String element : elements) {
			if (!isDateFormatValid) {
				break;
			}
			if ((element.length() > 2 && element.length() < 4) || element.length() > 4) {
				isDateFormatValid = false;
				break;
			} else {
				char[] chars = element.toCharArray();
				for (char c : chars) {
					if (!Character.isDigit(c)) {
						isDateFormatValid = false;
						break;
					}
				}
			}
		}
		return isDateFormatValid;
	}

	private double validateMilesPerLiter(String milesPerGallon) {
		{
			milesPerLiter = 0;
			try {
				double mpg = Double.parseDouble(milesPerGallon);
				milesPerLiter = mpg * gallonToLitreCF;
			} catch (NumberFormatException nfe) {
				milesPerLiter = 0;
			}
			return milesPerLiter;
		}

	}

	private double validateNumLtrsConsumed(String mileage, double milesPerLiter) {
		{
			double mlg = 0;
			double numberOfLitersConsumed = 0;
			try {
				mlg = Double.parseDouble(mileage);
				numberOfLitersConsumed = mlg / milesPerLiter;
			} catch (NumberFormatException nfe) {
				mlg = 0;
				numberOfLitersConsumed = 0;
			}
			return numberOfLitersConsumed;
		}

	}



	public Map readDataFromFile() {
		//File file = new File(filePath);
		List<String> eachLineData = new ArrayList<String>();
		Map<Date, FileData> fileDataMappedWithDate = new HashMap();
		try {
			//BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
			ClassPathResource classPathResource = new ClassPathResource("fuelPrices.txt");
			InputStream inputStream = classPathResource.getInputStream();
			LineNumberReader reader = new LineNumberReader(new InputStreamReader(inputStream));


				String line;
			int count = 0;
			try {
				while ((line = reader.readLine()) != null) {
					String[] str = line.split("\\s+");
					if (count > 0) {
						eachLineData.add(line);
					}
					if (str[0].equalsIgnoreCase("Date")) {
						++count;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw new ServiceException(101);
			}
			try {
				//fileReader.close();
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new ServiceException(101);
			}
		} catch (FileNotFoundException e) {
			System.out.println("file read error");
			e.printStackTrace();

			throw new ServiceException(111);
		}
		catch(IOException ie){
			System.out.println("io error");
			ie.printStackTrace();
			throw new ServiceException(101);
		}

		for (String str : eachLineData) {
			String[] split = str.split("\\s+");
			if(!str.isEmpty()) {
				try {
					Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(split[0]);
					double ULSP_Pump = Double.parseDouble(split[1]);
					double ULSD_Pump = Double.parseDouble(split[2]);
					double ULSP_Duty = Double.parseDouble(split[3]);
					double ULSD_Duty = Double.parseDouble(split[4]);
					double ULSP_Vat = Double.parseDouble(split[5]);
					double ULSD_Vat = Double.parseDouble(split[6]);
					FileData fileData = new FileData(date1, ULSP_Pump, ULSD_Pump, ULSP_Duty, ULSD_Duty, ULSP_Vat, ULSD_Vat);
					fileDataMappedWithDate.put(date1, fileData);
					//fuelPriceList.add(fileData);
				} catch (ParseException e) {
					System.out.println("Date error");
					e.printStackTrace();
					throw new ServiceException(100);
				}
			}
		}
		return fileDataMappedWithDate;
	}


}



