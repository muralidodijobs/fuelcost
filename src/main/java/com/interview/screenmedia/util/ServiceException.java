package com.interview.screenmedia.util;

import java.util.ResourceBundle;
/**
 * @author SA334475
 *
 */
public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = 6911109947576118163L;

	int statusCode;
	static ResourceBundle resourceBundle = ResourceBundle.getBundle("exception");

	public int getStatusCode() {
		return statusCode;
	}

	public ServiceException(int statusCode) {
		super(getMessage(statusCode));
		this.statusCode = statusCode;
	}

	public ServiceException(int statusCode, String message) {
		super(message);
		this.statusCode = statusCode;
	}

	private static String getMessage(int statusCode) {
		String message = null;
		try {
			message = resourceBundle.getString(String.valueOf(statusCode));
		} catch (Exception ex) {
			ex.getMessage();
		}
		return message;
	}

	public ServiceException getExceptionResponse() {
		return this;
	}
}
