package com.interview.screenmedia.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * @author SA334475
 *
 */
@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExceptionResponse {

	@XmlElement(name = "StatusCode" , required = true)
	private int statusCode;
	@XmlElement(name = "Message", required = true)
	private String message;

	public ExceptionResponse() {}

	public int getStatusCode() {
		return statusCode;
	}

	public String getMessage() {
		return message;
	}

	public ExceptionResponse(ServiceException ex){
		this.statusCode = ex.statusCode;
		this.message = ex.getMessage();
	}
}
