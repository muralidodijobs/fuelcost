package com.interview.screenmedia;

import javax.validation.Valid;

import com.interview.screenmedia.util.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.interview.screenmedia.entities.RequestModel;
import com.interview.screenmedia.util.ExceptionResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Api( tags="Fetch result", description = "Operations pertaining to fetch results")
public class Controller {
	private static final Logger logger = LoggerFactory.getLogger(Controller.class);
	//private static final
	@Autowired
	private Service service;
	
	@ApiOperation(value = "Fetch result",response = Object.class)
	@PostMapping("/fuelCost")
	//@RequestMapping(value = "/result", method = RequestMethod.POST)
	public  Object getResponseObj(@Valid @RequestBody RequestModel requestObject) {
		try {
		
		logger.info("Request Recieved");
		return service.calculateFuelCost(requestObject);
		}catch(ServiceException ex){
			logger.error("Exception: "+ex.getMessage());
			return new ExceptionResponse(ex);
		}
	}
	
}
