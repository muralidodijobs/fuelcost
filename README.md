1. Project title: FuelCost
Technology stack: Java, springboot and restful api
Build tool: Maven

2. Download the project from bitbucket control repository hosting service, from the below path:
git clone https://muralidodijobs@bitbucket.org/muralidodijobs/fuelcost.git

3. Import the downloaded code as a maven project into a IDE like (IntelliJ/Eclipse/NetBeans)

4. Build the project, go to project folder from command prompt and run the followed: mvn clean install or do it from IDE
itself and then from the IDE Run the FuelCostApplication from the followed path:
\src\main\java\com\interview\screenmedia\FuelCostApplication

5. Test the webservice from http client like postman (select post method from the drop down
and and give the url as http://localhost:8080/fuelCost and click on body choose Raw radio button and paste the
below request body or test with different kinds of invalid data to ascertain how validation works
{
	"date": "1/1/2018",
	"mpg": "65",
	"mileage": "425",
	"fuelType": "Petrol"
}
and make sure to select only JSON(application/json) format and not any other format from the drop down and click on send)

6. The webservice request is intercepted by the RestController class Controller and request is
processed by the implementation class Service both the classes, can be found under
src/main/java/com/interview/screenmedia

7. For the above mentioned classes the corresponding Junits are ControllerTest and ServiceTest
can be found under src/test/java/com/interview/screenmedia

8. Under resources directory the input file "fuelPrices.txt" is placed

9. Please ensure that the journey date and current date(which ever day you run the application)
both should be present and in the format as mentioned in the input file "fuelPrices.tx"

10. Validations are in place to handle exceptions thru custom exception class ServiceException
and the appropriate messages are published in exception.properties

11. The other properties file created is application.properties where server port(8080) details has been specified.

12. From command prompt go to the project directory and run the below outlined:
mvn clean install

13. From the same project directory  >cd target and run the below outlined:
\fuelcost\target>java -jar FuelCost-0.0.1-SNAPSHOT.jar

14. That's it! Also the application can be accessed from aws public host server, details as followed:
Test the webservice from http client like postman (select post method from the drop down
and and give the url as http://ec2-18-221-232-7.us-east-2.compute.amazonaws.com:8080/fuelCost and click on body
choose Raw radio button and paste thebelow request body or test with different kinds of invalid data to
ascertain how validation works
{
	"date": "1/1/2018",
	"mpg": "65",
	"mileage": "425",
	"fuelType": "Petrol"
}
and make sure to select only JSON(application/json) format and not any other format from the drop down and click on send)





